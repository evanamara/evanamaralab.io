---
title: 'New Website'
date: 2021-10-01
permalink: /posts/2021/10/new-website/
tags:
  - jekyll
  - gitlab
  - blogging
---

Welcome to my new blog and portfolio website!
## Who am I?
I'm Evan, a recent graduate from Central Connecticut State University with a bachelor’s degree in management information systems and specialization in information security and infrastructure.

Some of my areas of interest are:
* System administration
* Security and privacy
* Free and open source software
* Data storage, management, and compression

[More about me on the homepage](https://evanamara.com/)
## Creating this blog
### Project goals
There were a few goals and features I wanted to accomplish when creating this website:
* Simple design that works on multiple screen sizes
* Open source
* Encrypted and secured with TLS
* JavaScript optional
* Static site
* RSS

Considering these project goals, I determined that [Hugo](https://gohugo.io/) or [Jekyll](https://jekyllrb.com/) would be a good static site generator to use and that I would use GitLab Pages for hosting. Ultimately, I chose Jekyll over Hugo for its better documentation and that fact that I found a theme for it that I liked.
### Migrating a GitHub Pages theme to GitLab Pages
While looking at other websites for ideas of how I want the website to look, I discovered a great Jekyll theme on GitHub called [AcademicPages](https://github.com/academicpages/academicpages.github.io). After working with it for a little while and trying it out, I decided that it would fit perfectly for my website, with only a few minor changes needed. The first thing I had to do was migrate the GitHub repository to GitLab since I wanted the site to be hosted on GitLab Pages due to its better freely-available features and the fact that GitLab is an open source project.

I started by cloning the AcademicPages repo to a GitLab project and renamed the repo to the GitLab pages name format when being used inside a project (\<projectname>.gitlab.io). Cloning the repo to a project instead of just cloning it to your user has the benefit of allowing you to change the subdomain to whatever you want instead of it just being your GitLab username. Next, I added the default gitlab-ci.yml Jekyll template to the repository. GitLab then immediately built the website template theme and put it up at \<projectname>.gitlab.io without any need to modify the default gitlab-ci.yml file.
### Theme adjustments made
The AcademicPages theme was almost perfect out of the box, but I wanted to make a few minor changes to it:
* Added support for GitLab links on the sidebar
* Turned the Portfolio page into a Projects page and sorted the items by creation date
* Replaced any http links with https links 
* Removed pages that wouldn't be useful for me
* Replaced the CV page with a Resume link in the navigation bar that goes directly to a pdf of my public resume
* Disabled Google Analytics

### Setting a custom domain and HTTPS on GitLab Pages
When I was ready, I started looking at different domain name registrars. I went with [Namecheap](https://www.namecheap.com/) because their prices for a .com were the best I could find while also providing free WHOIS protection. After purchasing evanamara.com, I had to go to the Pages settings in the repo and add the custom domain name. I was surprised to see that GitLab now had a built in option for automatically getting a certificate from Let's Encrypt, enabled by default. Last time I used GitLab pages, you had to manually secure a site with Let's Encrypt by downloading certbot, exporting a certificate, and manually uploading it. It is much less annoying to do so now! All that was left was to add the following DNS records...
![Namecheap DNS records required for GitLab Pages](/images/blog/nc-advanced-dns.png)
The changes took a few hours to propagate and then everything was working fine! I used [whatsmydns.net's DNS Propagation Checker](https://www.whatsmydns.net/) to watch this happen in real time. After it propagated, all I did was press the recheck verification button. GitLab handled setting up the certificate by itself.
## What's next?
Overall, setting up the website was a lot easier than I expected it would be. I still have a few minor adjustments I want to make to the theme, which I intend to do within the coming few days. I'll also make a short blog post here when the changes are complete. Feel free to follow the RSS feed to be notified of any new posts. I have some ideas already for posts I want to write.
