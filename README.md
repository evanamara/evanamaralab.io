# evanamara.gitlab.io
This repository holds the source to my personal blog and portfolio website hosted on GitLab Pages. The website is based off of the [AcademicPages](https://github.com/academicpages/academicpages.github.io) GitHub Pages template and is generated using [Jekyll](https://jekyllrb.com/), an open source static site generator.

## Directory Structure
Folder		| Description
-----------	| -----------
assets		| Static site assets (CSS, fonts, JS).
_data		| Data for navbar, language options, and comments.
_drafts		| Drafts for blog posts before they are published.
files		| Static files that are not images (Ex. pdf, zip).
images		| Image assets for the entire site.
_includes	| Partial html files that can be combined together by layouts.
_layouts	| The templates that wrap posts.
_pages		| Static pages.
_posts		| Blog posts that have been published.
_projects	| Projects that I have worked on.
_sass		| sass parials that get combined into the main CSS.
_site		| Where the generated site is placed.

## License
Website source is licensed under the MIT license. Website content is licensed under [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/).
