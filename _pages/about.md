---
permalink: /
title: "About"
excerpt: "About me"
author_profile: true
redirect_from: 
  - /about/
  - /about.html
---

Me
======
Hi there! My name is Evan. I am a recent graduate with a bachelor's degree in management information systems and specialization in information security and infrastructure.

This website
======
This website serves to hold my blog where I will write about technology, a list of projects I have worked on, and a copy of my resume. I launched this website on October 1st, 2021. The website is based off of the [AcademicPages](https://github.com/academicpages/academicpages.github.io) GitHub Pages template and is generated using [Jekyll](https://jekyllrb.com/), an open source static site generator. The website's source is licensed under the MIT license while the content is licensed under [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/).

Contact info
======
I am not a big fan of social media. Nonetheless, you can still contact me a few different ways:
* Email: [evan.amara@outlook.com](mailto:evan.amara@outlook.com)
* LinkedIn: [https://www.linkedin.com/in/evan-amara](https://www.linkedin.com/in/evan-amara)

Support
======
Want to thank me for my work or support me financially? [There are multiple ways to do so here.](https://coindrop.to/evan-amara)

Thank you!
