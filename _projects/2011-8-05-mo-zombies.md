---
title: "Mo' Zombies Minecraft Mod"
excerpt: "Game mod for Minecraft beta 1.7.3 to release 1.7.10. Adds more zombies to the game, each with unique abilities. Programmed in Java, released as open source."
date: 2011-8-05
collection: projects
---

Game mod for Minecraft beta 1.7.3 to release 1.7.10. Adds more zombies to the game, each with unique abilities. Programmed in Java, released as open source under the GPLv3 license. The mod had over 1 million downloads over its lifetime when I was actively developing it.

Source code: [https://gitlab.com/ehea617/mo-zombies](https://gitlab.com/ehea617/mo-zombies)

Downloads: [https://mega.nz/#F!UUZg3Yra!ymf3N81VIBCwHpJvjlpWvw](https://mega.nz/#F!UUZg3Yra!ymf3N81VIBCwHpJvjlpWvw)
