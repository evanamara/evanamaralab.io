---
title: "Web Archiver Chrome Extension"
excerpt: "Google Chrome extension that adds a button to the browser allowing you to save a webpage to the Internet Archive."
date: 2014-12-23
collection: projects
---

Google Chrome extension that adds a button to the browser allowing you to save a webpage to the Internet Archive.

Source code: [https://gitlab.com/ehea617/web-archiver-app](https://gitlab.com/ehea617/web-archiver-app)

Chrome Web Store: [https://chrome.google.com/webstore/detail/web-archiver/gjpgpobcdndcdcidmgcmlphbomllapjp](https://chrome.google.com/webstore/detail/web-archiver/gjpgpobcdndcdcidmgcmlphbomllapjp)
