---
title: "Friendship Group of Hartford Website"
excerpt: "Website for the nonprofit organization, Friendship Group of Hartford. I worked as the project manager and a developer along with three other people for the class MIS-300 - Project Management for Business at CCSU."
date: 2020-11-17
collection: projects
---

Website for the nonprofit organization, Friendship Group of Hartford. I worked as the project manager and a developer along with three other people for the class MIS-300 - Project Management for Business at CCSU.

URL: [https://friendshipgrouphtfrd.wixsite.com/home](https://friendshipgrouphtfrd.wixsite.com/home)
